#!/usr/bin/env python3
#
# pip3 install psycopg2-binary tabulate
#

import argparse
import psycopg2 as pg
import sys
from uuid import UUID
from tabulate import tabulate

DB_DATA = {
    "host": "localhost",
    "database": "cortirio_demo",
    "user": "cortirio",
    "password": "c0rt1r10"
}
VERSION = 0.1

tabulate_opts = {"tablefmt": "presto"}

avail_queries = {
    "headers_sim": "SELECT * FROM Simulation LIMIT 0",
    "all_active_sim": "SELECT * FROM Simulation WHERE status != 'COMPLETED'",
    "all_sim": "SELECT * FROM Simulation",
    "query_sim": "SELECT * FROM Simulation WHERE sim_id = %s",
    "last_sim": "SELECT * FROM Simulation ORDER BY start_time LIMIT %s",
    "proc_linked": "SELECT * FROM Process WHERE sim_fk = %s",
}
query_desc = {
    "all_active_sim": "show all the currently active simulations",
    "all_sim": "show all the simulations inside the Database",
    "query_sim": "show a specific simulation",
    "last_sim": "show a list of the latest simulations",
}

simul_headers = ("Simulation ID", "Pipeline ID", "Docker Name", "Aws Region", "Aws Batch Job Queue", "Workdir",
                 "Log dir", "Status", "Start time", "End time")
proc_headers = ("Task ID", "Simulation ID", "Process name", "Status", "Hash dir",
                "Submission time", "Start time", "Completion time", "Container name", r"%Cpu",
                r"%Mem", "Virtual mem (B)", "Peak Vmem (B)", "Walltime (ms)", "Exit code",
                "#CPUs req", "Time req", "Disk space req", "Memory req", "#Attempts")


class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    UNDERLINE = '\033[4m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    ENDC = '\033[0m'


def run_query(query, *params, conn, cur):
    cur.execute(query, params)
    return cur.fetchall()


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Execute queries on the DB of NextFlow statistics.")
    parser.add_argument("-v", "--version", action="version", version="%(prog)s " + str(VERSION))

    parser.add_argument("jobid", metavar="UUID", default="__all_active__", nargs="?",
                        help=("Return details of all *active* simulations, or details of a "
                              "specific simulation [UUID].")
                        )

    sim_opts = parser.add_argument_group("Simulations options")
    sim_opts.add_argument("-a", "--all-sims", dest="all_sim", action="store_true",
                          help="Return details of all the simulations in the database.")
    sim_opts.add_argument("-l", "--last", dest="last_sim", metavar="N", type=int,
                          const=15, nargs="?",
                          help="Return details of last N simulations (default: %(const)s).")
    sim_opts.add_argument("-L", "--last-one", dest="last_one", action="store_true",
                          help="Return details of the last simulation. Faster than -l1.")

    # proc_opts = parser.add_argument_group("Processes options")
    # proc_opts.add_argument(
    #     "-ps",
    #     "--proc-status",
    #     dest="proc_status",
    #     metavar="UUID",
    #     nargs=1,
    #     help="Return status of a specific process.")

    filter_opts = parser.add_argument_group("Filtering options")
    filter_opts.add_argument(
        "--from",
        metavar="DATE",
        nargs=1,
        help="Limit results to processes starting from this date.")
    filter_opts.add_argument(
        "--to",
        metavar="DATE",
        nargs=1,
        help="Limit results to processes ending before this date.")

    args = parser.parse_args()
    return args


def choose_query(args):
    if args.all_sim is True:
        return "all_sim"
    elif args.last_sim is not None:
        return "last_sim"
    elif args.jobid is not None:
        # Return the simulations where status != COMPLETED.
        if args.jobid == "__all_active__":
            return "all_active_sim"

        else:
            try:
                sim_id = str(UUID(args.jobid))
            except Exception:
                print("Error, the id of the simulation '{}' is not a valid "
                      "UUID, exiting.".format(args.jobid))
                sys.exit(1)
            return "query_sim"


def apply_query(query_rule, args, **params):
    if query_rule == "all_sim":
        return run_query(avail_queries[query_rule], **params)
    elif query_rule == "last_sim":
        return run_query(avail_queries[query_rule], args.last_sim, **params)
    elif query_rule == "all_active_sim":
        return run_query(avail_queries[query_rule], **params)
    elif query_rule == "query_sim":
        return run_query(avail_queries[query_rule], str(UUID(args.jobid)), **params)


if __name__ == "__main__":
    args = parse_arguments()

    with pg.connect(**DB_DATA) as conn:
        with conn.cursor() as cur:
            params = {"conn": conn, "cur": cur}

            query_rule = choose_query(args)
            simul_data_rows = apply_query(query_rule, args, **params)
            print("{}\nList of Simulations recorded, matching query {}'{}'{}:\n{}\n"
                  .format(colors.OKGREEN, colors.BOLD, query_desc[query_rule], colors.ENDC,
                          tabulate(simul_data_rows, headers=simul_headers, **tabulate_opts))
                  )

            if len(simul_data_rows) == 1:
                # Get all processes connected to the only simulation found/requested.
                simulation_id = simul_data_rows[0][0]
                processes = run_query(avail_queries["proc_linked"],
                                      simulation_id, **params)
                print("{}\nList of Processes that compose Simulation {}'{}'{}:\n{}\n"
                      .format(colors.OKGREEN, colors.BOLD, simulation_id, colors.ENDC,
                              tabulate(processes, headers=proc_headers, **tabulate_opts))
                      )
