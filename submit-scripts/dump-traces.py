#!/usr/bin/env python3
#
# Capture json coming from NF weblog and save them into pg.
# Make sure the following is inserted in `nextflow.config` to collect all traces:
#
#   trace {
#       enabled = true
#   }
#
#
# To install the needed packages: `pip3 install quart asyncpg`
#

from quart import Quart, request
from datetime import datetime
import json
import asyncpg

app = Quart(__name__)

VERBOSE = False


@app.route("/", methods=["POST"])
async def index():
    data = await request.get_json()
    if not data:
        return

    # TODO: Avoid creating a new connection at each request.
    # Maybe with a connection pool:
    # https://magicstack.github.io/asyncpg/current/usage.html#connection-pools
    conn = await asyncpg.connect("postgresql://cortirio@localhost/cortirio_demo")

    print(json.dumps(data, indent=4, sort_keys=True)) if VERBOSE else None

    if data["event"] == "started":
        await conn.execute(
            '''INSERT INTO Simulation
                (sim_id, pipeline_fk, docker_name, aws_region, aws_batch_jq, aws_s3_workdir,
                 aws_s3_logdir, status, start_time, end_time)
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)''',
            data["runId"],
            "92a44271-d811-40d0-939d-958515391e7d",
            "docker name",
            "region name",
            "jq name",
            None,  # workdir, populated when the first process is submitted, see below.
            "s3://path/",
            data["runStatus"].upper(),
            datetime.fromisoformat(data["utcTime"][:-1]) if data["utcTime"][-1] == "Z" else None,
            None,
        )

    elif data["event"] == "completed":
        await conn.execute(
            '''UPDATE Simulation
               SET status = $1, end_time = $2
               WHERE sim_id = $3''',
            data["runStatus"].upper(),
            datetime.fromisoformat(data["utcTime"][:-1]) if data["utcTime"][-1] == "Z" else None,
            data["runId"],
        )

    elif data["event"] == "process_submitted":

        # The common workdir is contained in trace["workdir"], just before trace["hash"].
        row = await conn.fetchrow(
            '''SELECT aws_s3_workdir
               FROM Simulation
               WHERE sim_id = $1''', data["runId"])

        if row["aws_s3_workdir"] is None:
            wd = data["trace"]["workdir"].split(data["trace"]["hash"], 1)[0]
            await conn.execute(
                '''UPDATE Simulation
                   SET aws_s3_workdir = $1
                   WHERE sim_id = $2''',
                wd,
                data["runId"],
            )

        fields = ("task_id", "sim_fk", "name", "status", "hash_dir", "submit_time", "start_time",
                  "end_time", "docker_name", "cpu_perc", "mem_perc", "vmem", "peak_vmem",
                  "walltime", "exit_status", "cpu_num", "time_requested", "disk_requested",
                  "memory_requested", "attempts_num")
        await conn.execute(
            '''INSERT INTO Process ({}) VALUES ({})'''.format(
                ", ".join(fields),
                ", ".join("$"+str(i) for i in range(1, len(fields)+1))
            ),
            data["trace"]["task_id"],       # task_id
            data["runId"],                  # sim_fk
            data["trace"]["name"],          # name
            None,                           # status
            data["trace"]["hash"],          # hash_dir
            datetime.fromtimestamp(data["trace"]["submit"] // 1000),
            None,                           # start_time
            None,                           # end_time
            data["trace"]["container"],     # docker_name
            None,                           # cpu_perc
            None,                           # mem_perc
            None,                           # vmem
            None,                           # peak_vmem
            None,                           # walltime
            None,                           # exit_status
            data["trace"]["cpus"],          # cpu_num
            data["trace"]["time"],          # time_requested
            data["trace"]["disk"],          # disk_requested
            data["trace"]["memory"],        # memory_requested
            None,                           # attempts_num
        )

    elif data["event"] == "process_started":
        await conn.execute(
            '''UPDATE Process
               SET start_time = $1, status = $2
               WHERE task_id = $3 and sim_fk = $4''',
            datetime.fromtimestamp(data["trace"]["start"] // 1000),
            data["trace"]["status"],        # status
            data["trace"]["task_id"],       # task_id
            data["runId"],                  # sim_fk
        )

    elif data["event"] == "process_completed":
        fields_upd = ("status", "end_time", "cpu_perc", "mem_perc", "vmem",
                      "peak_vmem", "walltime", "exit_status", "attempts_num")
        fields_to_id = ', '.join(fld + " = $" + str(ind+1) for ind, fld in enumerate(fields_upd))
        await conn.execute(
            '''UPDATE Process
               SET {}
               WHERE task_id = ${} and sim_fk = ${}'''.format(fields_to_id,
                                                              len(fields_upd) + 1,
                                                              len(fields_upd) + 2),
            data["trace"]["status"],        # status
            datetime.fromtimestamp(data["trace"]["complete"] // 1000),
            data["trace"].get(r"%cpu"),     # cpu_perc
            data["trace"].get(r"%mem"),     # mem_perc
            data["trace"].get("vmem"),          # vmem
            data["trace"].get("peak_vmem"),     # peak_vmem
            data["trace"]["realtime"],      # walltime
            data["trace"]["exit"],          # exit_status
            data["trace"]["attempt"],       # attempts_num

            data["trace"]["task_id"],       # task_id
            data["runId"],                  # sim_fk
        )

    await conn.close()
    return "done!"


if __name__ == "__main__":
    app.run()
