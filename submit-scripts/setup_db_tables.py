#!/usr/bin/env python3
#
# yum install postgresql-server
# pip3 install psycopg2-binary
#

import psycopg2 as pg
from psycopg2.extensions import AsIs

RESET = True

DB_HOST = "localhost"
DB_NAME = "cortirio_demo"
DB_USER = "cortirio"
DB_PSWD = "c0rt1r10"


def connection_and_cursor():
    if RESET:
        # Log in to the "base" db and drop the old db.
        conn = pg.connect(host=DB_HOST, database="postgres", user=DB_USER, password=DB_PSWD)
        conn.autocommit = True
        conn.cursor().execute("DROP DATABASE IF EXISTS %s;", (AsIs(DB_NAME), ))
        conn.cursor().execute("CREATE DATABASE %s WITH OWNER %s;", (AsIs(DB_NAME), AsIs(DB_USER)))
        conn.autocommit = False

    # Connect to the newly re-created db.
    conn = pg.connect(host=DB_HOST, database=DB_NAME, user=DB_USER, password=DB_PSWD)
    return conn, conn.cursor()


def create_tables(cur):
    cur.execute("CREATE TYPE SIM_STATUS AS ENUM ('STARTED', 'RUNNING', 'COMPLETED', 'FAILED');")
    cur.execute(
        "CREATE TYPE PROC_STATUS AS ENUM ('SUBMITTED', 'RUNNING', 'COMPLETED', 'FAILED');")
    cur.execute("""CREATE TABLE Pipeline(pipeline_id UUID PRIMARY KEY,
                                         git_repo_name VARCHAR,
                                         git_commit CHAR(40),
                                         git_branch VARCHAR
                                        );""")

    cur.execute("""CREATE TABLE Simulation(sim_id UUID PRIMARY KEY,
                                           pipeline_fk UUID REFERENCES Pipeline(pipeline_id),
                                           docker_name CHAR(100),
                                           aws_region CHAR(30),
                                           aws_batch_jq CHAR(100),
                                           aws_s3_workdir VARCHAR,
                                           aws_s3_logdir VARCHAR,
                                           status SIM_STATUS,
                                           start_time TIMESTAMP,
                                           end_time TIMESTAMP
                                          );""")

    # walltime/realtime is in milliseconds.
    cur.execute("""CREATE TABLE Process(task_id SMALLINT,
                                        sim_fk UUID REFERENCES Simulation(sim_id),
                                        name VARCHAR,
                                        status PROC_STATUS,
                                        hash_dir CHAR(10),
                                        submit_time TIMESTAMP,
                                        start_time TIMESTAMP,
                                        end_time TIMESTAMP,
                                        docker_name CHAR(100),
                                        cpu_perc REAL,
                                        mem_perc REAL,
                                        vmem INT,
                                        peak_vmem INT,
                                        walltime INT,
                                        exit_status INT,
                                        cpu_num SMALLINT,
                                        time_requested INTERVAL,
                                        disk_requested DOUBLE PRECISION,
                                        memory_requested DOUBLE PRECISION,
                                        attempts_num SMALLINT,
                                        PRIMARY KEY (task_id, sim_fk)
                                       );""")

    # Example Pipeline, until we create the wrapper that reads the git commit.
    data = ('92a44271-d811-40d0-939d-958515391e7d', 'asd', 'asdf', 'dfgsgd')
    cur.execute("""INSERT INTO Pipeline VALUES (%s, %s, %s, %s);""", data)


if __name__ == "__main__":
    try:
        conn, cur = connection_and_cursor()
        create_tables(cur)
    finally:
        conn.commit()
