## How to setup AWS Batch

Remember to set a security group in EC2.


### User setup

Login with your main/root account, then create a new account with limited and fewer permissions.

Follow these instructions:
https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html
to create a `cortirio` account.

Go to the IAM service -> Users -> Add user.

Set a username and select the access type, choose both the `Programmatic access` to obtain an access key to be added in the nextflow config later and the `AWS Management Console access`, so the user can access with a password and see e.g. their jobs running or not.

Do not add the user to a group, but select "Attach existing policies directly", search for the following:

  - AmazonS3FullAccess
  - AmazonEC2FullAccess
  - AWSBatchFullAccess

More fine-grained policies (e.g., allow R/W only to a certain bucket, allow submissions only to a certain queue) can be done by creating custom IAM policies.

[Here](https://apeltzer.github.io/post/01-aws-nfcore/) they say that if you want to use spot instances in your compute environment, you have to add the following roles/policies (untested):

  - AWSServiceRoleForEC2SpotFleet
  - AmazonEC2SpotFleetRole


Once the user has been created, it can login from a specific address: https://440211689078.signin.aws.amazon.com/console/ (where `440211689078` is a ID unique to the root account), username `cortirio` and password the one set.

Copy the user access key and secret key, and put those in the `nextflow.config` in the aws batch profile.


### Setup AWS Batch

https://docs.aws.amazon.com/batch/latest/userguide/what-is-batch.html
https://docs.aws.amazon.com/batch/latest/userguide/Batch_GetStarted.html


Visit https://console.aws.amazon.com/batch/home?#/dashboard

#### Compute environments

Visit `Compute environments` on the left of the AWS Batch dashboard.

Give a name to the queue, let it be Managed by AWS, set the `Service role` to `AWSBatchServiceRole` (or leave it empty to let AWS create it), set `Instance role` to `ecsInstanceRole` (or leave it empty to let AWS create it), set a key pair if you have one added.

Choose the family of Virtual Machines to use.

Then you can choose whether to use On-demand instances, where the price per-hour is fixed and scales with the number of hours used, or Spot instances, where you set a percentage of the on-demand price and when that price threshold is met the instances will be spawned; if resources are requested by AWS, they can be requested with a warning of 2 minutes (see this article on how to discover whether the warning has been issued: https://autoscalr.com/2017/09/26/aws-spot-instance-two-minute-warning-detect/).

You can visit the spot instance advisor to select the family of machines to use, depending e.g. on the pro:
https://aws.amazon.com/ec2/spot/instance-advisor/

Define a minimum and maximum number of vCPU that will be running in the CE; a `Desired vCPU` number is available, to have that number of vCPUs always running, so that jobs can be created immediately without waiting for a fresh VM to be initialized.

You can setup multiple Compute environments, e.g. one for critical data that must be processed immediately


#### Job queue

A job queue has a name, a priority (the higher the number, the higher the precedence the jobs of that queue will have) and belong to at least one CE


#### Other settings

NextFlow doesn't need other settings, because it will create Job definitions on its own.
You should be able to define your job definition by hand as well and indicate it to nextflow: in particular is useful if you want to set a higher-than-one number of attempts to run a job (in case e.g. a spot instance gets terminated by AWS).
