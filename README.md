## Tutorial on NextFlow - Local and AWS setup

For instructions on how to setup the AWS infrastructure, refer to [the separate guide](./aws_batch_setup.md).


### Repository setup on `C3HPC` infrastructure

In order to test the execution of NextFlow on a Linux machine, we setup a user `pmacey` on `C3HPC`, the High Performance Computing infrastructure managed by eXact lab.

From your personal computer, use a `Secure SHell` (`SSH`) to access the login node (password `3s5_XFu4EkBbTy+3`):
```console
$ ssh pmacey@login.c3hpc.exact-lab.it
```

Download this repository on the computing node of `C3HPC`, by using `git` (password `KgPN8ce537QV`):
```console
$ git clone https://exactlab-demo@bitbucket.org/exactlab/nf-pipes.git

$ cd nf-pipes/demo/
```

Java >= 1.8 is already installed, so proceed to download and install the latest NextFlow executable with:
```console
$ curl -s https://get.nextflow.io | bash
```

This should produce an executable file named `nextflow` in your current directory, which you can use to run a hello world example:
```console
$ ls -alh nextflow
-rwx--x--x 1 pmacey cortirio 15K Feb 14 12:27 nextflow

$ ./nextflow run hello
```

### NextFlow local execution using docker

In order to use docker you need to request an interactive node where docker is available:
```console
$ qsub -l nodes=b13:ppn=24 -I
```

You can now use docker to run a containerized job with all the needed libraries installed:
```console
$ cd nf-pipes/demo/
$ ./nextflow run main.nf -profile localDocker
N E X T F L O W  ~  version 19.01.0
Launching `main.nf` [intergalactic_curie] - revision: 993ffc1a7f

R N A S E Q - N F   P I P E L I N E
===================================
transcriptome: nf-pipes/demo/data/ggal/transcriptome.fa
reads        : nf-pipes/demo/data/ggal/*_{1,2}.fq
outdir       : nf-pipes/demo/results

[warm up] executor > local
[52/e6b6af] Submitted process > fastqc (FASTQC on lung)
[c4/8104e5] Submitted process > fastqc (FASTQC on liver)
[00/d54b2c] Submitted process > index
[8d/0a4e7c] Submitted process > fastqc (FASTQC on gut)
[53/cdd247] Submitted process > quantification (lung)
[bc/f75584] Submitted process > quantification (liver)
[97/82364c] Submitted process > quantification (gut)
[07/3352bc] Submitted process > multiqc

Done! Open the following report in your browser --> nf-pipes/demo/results/multiqc_report.html
```

You can now copy the output report back from `C3HPC` to your personal computer (e.g. with `scp`) in order to visualize the output report from the example application.


### NextFlow execution on AWS Batch

Still inside the `C3HPC` infrastructure, you can launch a job on AWS, using the AWS Batch service.

Visit the dashboard we prepared on AWS:

1. access the login page at https://440211689078.signin.aws.amazon.com/console
2. use username `cortirio` and password `u4N}-'Gp*4YR`
3. reach the dashboard of the AWS Batch service at https://eu-central-1.console.aws.amazon.com/batch/home?/dashboard
4. you should see an empty Job Queue and a Compute Environment with a certain maximum amount of vCPU available for it.

As soon as the Job Queue will start receiving jobs, **new EC2 VMs will be initialized** and start running your jobs; in a matter of minutes since the jobs are finished, the VMs used will be terminated.
As soon as the VMs are initialized, the Jobs will move from status `Runnable` to `Running`, and eventually to `Succeeded`.

You can then **watch live** the creation of the EC2 virtual machines at the dashboard containing the server instances: https://eu-central-1.console.aws.amazon.com/ec2/v2/home?#Instances:sort=desc:launchTime

We uploaded the same data available inside the `data/` directory of this repo on AWS S3 (Simple Storage Service), and we already setup the AWS API key in the NextFlow configuration file; therefore, to submit jobs on AWS Batch you just need to execute:

```console
$ ./nextflow run main.nf -profile batch                                                 \
        --reads         's3://test-exactlab/cortirio-demo/data/ggal/*_{1,2}.fq'         \
        --transcriptome 's3://test-exactlab/cortirio-demo/data/ggal/transcriptome.fa'   \
        --outdir        's3://test-exactlab/cortirio-demo/results'

N E X T F L O W  ~  version 19.01.0
Launching `main.nf` [gloomy_ritchie] - revision: f3aa5672d0

R N A S E Q - N F   P I P E L I N E
===================================
transcriptome: s3://test-exactlab/cortirio-demo/data/ggal/transcriptome.fa
reads        : s3://test-exactlab/cortirio-demo/data/ggal/*_{1,2}.fq
outdir       : s3://test-exactlab/cortirio-demo/results

[warm up] executor > awsbatch
[63/38b741] Submitted process > index
[e7/0e01d9] Submitted process > fastqc (FASTQC on lung)
[f8/a590db] Submitted process > fastqc (FASTQC on liver)
[3f/f48807] Submitted process > fastqc (FASTQC on gut)
[52/9dff94] Submitted process > quantification (liver)
[c3/dfcb23] Submitted process > quantification (lung)
[ae/da3122] Submitted process > quantification (gut)
[1e/49fea0] Submitted process > multiqc

Done! Open the following report in your browser --> s3://test-exactlab/cortirio-demo/results/multiqc_report.html
```

To **visit the report html page** built in the example visit https://s3.console.aws.amazon.com/s3/object/test-exactlab/cortirio-demo/results/multiqc_report.html?region=eu-central-1&tab=overview and click on the `Open` button.


In the NextFlow configuration file, we choose a public docker (available on the docker hub at https://hub.docker.com/r/bebosudo/rnaseq-nf-awscli) to be run inside the EC2 VMs: this is the same docker used in the example above on the cluster, with the addition of the AWS CLI suite, which enables NextFlow to interact with the AWS S3 storage service.

The current Compute Environment we prepared uses "On demand" instances: this means that the VMs will be initialized immediately after AWS Batch receives a new job (in order not to wait for resources).
There is also the "Spot instances" VM initialization mode available, which uses unused computing infrastructure available at AWS: a specific threshold can be set, in order to save up to 90% wrt "On demand" VMs.


### Resume jobs

No matter where you execute NextFlow, an important intrinsic feature is the ability to resume the execution of a job.
This is achieved by using hashes of the intermediate processes in a pipeline.

To reuse cached jobs add the `-resume` flag to the nextflow run command line, e.g. here we submit a job and then we request the same job, but we allow the resume of already computed layers from the work/cache directory:
```console
$ ./nextflow run main.nf -profile localDocker
...

[warm up] executor > local
[cb/f94ead] Submitted process > index
[c3/725b6d] Submitted process > fastqc (FASTQC on liver)
[39/fb3495] Submitted process > fastqc (FASTQC on gut)
[78/bbc94e] Submitted process > fastqc (FASTQC on lung)
[c4/7c8188] Submitted process > quantification (lung)
[74/ff4015] Submitted process > quantification (liver)
[b8/39d9c3] Submitted process > quantification (gut)
[40/bf16ff] Submitted process > multiqc

$ ./nextflow run main.nf -profile localDocker -resume
...

[warm up] executor > local
[cb/f94ead] Cached process > index
[c3/725b6d] Cached process > fastqc (FASTQC on liver)
[39/fb3495] Cached process > fastqc (FASTQC on gut)
[78/bbc94e] Cached process > fastqc (FASTQC on lung)
[c4/7c8188] Cached process > quantification (lung)
[74/ff4015] Cached process > quantification (liver)
[b8/39d9c3] Cached process > quantification (gut)
[40/bf16ff] Cached process > multiqc

```

Notice how the hashes are the same.
These hashes represent directories inside the working directory, and we can see that there are some files stored in there, e.g. for the `index` process phase, reported as `cb/f94ead` in the NextFlow execution, we can see a directory named `index` as the name of the process, a link to the input file, and some internal NextFlow files:

```console
$ ls -alhd work/cb/f94ead*
drwxr-xr-x 3 pmacey cortirio 4.0K Feb 15 11:08 work/cb/f94ead01ccc3183750652a089072b7

$ ls -alh work/cb/f94ead*
drwxr-xr-x 3 pmacey cortirio 4.0K Feb 15 11:08 .
drwxr-xr-x 3 pmacey cortirio 4.0K Feb 15 11:08 ..
-rw-r--r-- 1 pmacey cortirio    0 Feb 15 11:08 .command.begin
-rw-r--r-- 1 pmacey cortirio 1.1K Feb 15 11:08 .command.err
-rw-r--r-- 1 pmacey cortirio 1.1K Feb 15 11:08 .command.log
-rw-r--r-- 1 pmacey cortirio    0 Feb 15 11:08 .command.out
-rw-r--r-- 1 pmacey cortirio 2.2K Feb 15 11:08 .command.run
-rw-r--r-- 1 pmacey cortirio   70 Feb 15 11:08 .command.sh
-rw-r--r-- 1 pmacey cortirio    1 Feb 15 11:08 .exitcode
drwxr-xr-x 2 root   root     4.0K Feb 15 11:08 index
lrwxrwxrwx 1 pmacey cortirio   59 Feb 15 11:08 transcriptome.fa -> ../../../data/ggal/transcriptome.fa
```

#### Always use `-resume`

The `-resume` flag applied to a project that was never executed is ignored, so it's safe to always use it: if a project is new, no cache will be used.
```console
$ ./nextflow run main.nf -profile localDocker -resume

WARN: It appears you have never run this project before -- Option `-resume` is ignored
...
```

If no `-resume` flag is passed to NextFlow, the processes will restart computing all the layers of the chain.


### Recompute only the missing processes

NextFlow has the ability to recognize when parts of a pipeline are already computed or are missing.

For example, let's compute the RNA sequence only for the lung:
```console
$ ./nextflow run main.nf -profile localDocker \
        --reads "/u/cortirio/pmacey/nf-pipes/demo/data/ggal/lung_{1,2}.fq"

...
reads        : /u/cortirio/pmacey/nf-pipes/demo/data/ggal/lung_{1,2}.fq         <----------

[warm up] executor > local
[40/2e0d48] Submitted process > fastqc (FASTQC on lung)
[47/5739c5] Submitted process > index
[fe/b9af0c] Submitted process > quantification (lung)
[bc/818298] Submitted process > multiqc
```

If we now have more input to work on (`gut` and `liver` input files), we can :
```console
$ ./nextflow run main.nf -profile localDocker \
        --reads "/u/cortirio/pmacey/nf-pipes/demo/data/ggal/*_{1,2}.fq" \
        -resume

...
reads        : /u/cortirio/pmacey/nf-pipes/demo/data/ggal/*_{1,2}.fq            <----------

[warm up] executor > local
[47/5739c5] Cached process > index
[40/2e0d48] Cached process > fastqc (FASTQC on lung)
[f0/24719a] Submitted process > fastqc (FASTQC on liver)
[97/a93007] Submitted process > fastqc (FASTQC on gut)
[fe/b9af0c] Cached process > quantification (lung)
[e7/e8f4bc] Submitted process > quantification (gut)
[aa/a1d66a] Submitted process > quantification (liver)
[a2/fc1a81] Submitted process > multiqc
```

As you can see, the processes regarding the lung were already computed and cached in the workind directory, and therefore were skipped and retrieved from cache: this can be achieved only when (part of) a pipeline is composed of embarassingly parallel processes, such as in the example pipeline we provided.


### Singularity support for environments with security constraints

Docker containers are great if there are no costraints in the execution environment regarding security; this is because with a docker container the user can easily mount the host system and obtain root access.

In the High Performance Computing eco-system, an alternative container solution has been developed, called [Singularity](https://www.sylabs.io/singularity/), which promises to bring the powerfulness of containers to shared environments where docker cannot be provided to users.

A profile has been created to allow the use of singularity, which is able to download docker containers and convert them into its own container format, so the execution flow is the same as with the docker profile.

```console
$ ./nextflow run main.nf -profile localSingularity
...

[warm up] executor > local
[5e/5f19f5] Submitted process > index
[f5/960c7e] Submitted process > fastqc (FASTQC on lung)
[4e/2516b4] Submitted process > fastqc (FASTQC on gut)
[e6/4d311f] Submitted process > fastqc (FASTQC on liver)
[3a/cc566f] Submitted process > quantification (liver)
[d3/d642e5] Submitted process > quantification (gut)
[ea/d26bbe] Submitted process > quantification (lung)
[5f/05735f] Submitted process > multiqc
```

Singularity 3.0.0 is required, which can be installed from source or [from RPM](http://springdale.math.ias.edu/data/puias/computational/7/x86_64//singularity-3.0.1-1.sdl7.x86_64.rpm).


### Storing intermediate processes

In order to store intermediate processes and be able to resume projects, you need to store the work directory where partial processes are saved. If not stated, by default NextFlow saves partial files in a directory `work/` in the current directory where you launched the pipeline.

In the case of a remote job submission to AWS Batch, the workdir is defined in the `nextflow.config` file, and for this demo it points to a directory inside an S3 bucket `s3://test-exactlab/workdir` in region `eu-central-1`. As long as that directory is not deleted or tampered, NextFlow is able to restore previous partial processes, upon request at submission, using the `-resume` flag.

