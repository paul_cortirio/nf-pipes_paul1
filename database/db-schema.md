## DB schema for storing

![db-schema.png](db-schema.png)

Created on https://www.yworks.com/yed-live/, reload the work with the file `db-schema.graph-ml` in this repo.


* Table `ProcessingChain`:
  * git repo name/url
  * git commit hash;
  * git branch


* Table `Simulation`:
  * Foreign key to Pipeline.git commit hash;
  * user who submitted the simulation
  * number of processes (simply count the number of processes below)
  * docker container hash
  * aws batch job queue
  * aws region
  * aws s3 bucket partial job dir  // Intermediate results are saved in the cache dir on S3
  * dir on s3 (with uuid) where input log is stored
  * dir on s3 (with uuid) where output log is stored
  * nextflow version and revision? (already present in the output log)


* Table `Process`:
  * foreign key: parentSimulation
  * startDate
  * endDate
  * status
