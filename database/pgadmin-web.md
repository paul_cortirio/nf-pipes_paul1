## pgAdmin 4 web interface

pgAdmin version 4 has been installed on the demo VM.

### Login

Point your web browser to: http://18.185.125.175, use username `demo@cortirio` with password `3s5_XFu4EkBbTy+3`.

A database named `NF` has been setup (pointing to localhost, the same VM pgAdmin is running from), and is available in the dashboard on the left, under `Servers`.

To visualize the data inside the tables, navigate to Schemas > public > Tables. Right click on a table > `View/Edit data`.


### Installation

```console
# yum install gcc python-devel
# wget https://ftp.postgresql.org/pub/pgadmin/pgadmin4/v4.2/pip/pgadmin4-4.2-py2.py3-none-any.whl
# pip3 install pgadmin4-4.2-py2.py3-none-any.whl

# cp /usr/local/lib/python3.7/site-packages/pgadmin4/config{,_local}.py
```

Edit `/usr/local/lib/python3.7/site-packages/pgadmin4/config_local.py` changing port (set `80` so ) and ip (set `0.0.0.0`):
```
# screen
# python3 /usr/local/lib/python3.7/site-packages/pgadmin4/pgAdmin4.py
```
