AWS AMI 2 used, so it's a bit different than Centos/RHEL

```
# yum install postgresql-server python-psycopg2

# initdb -D /var/lib/pgsql/data/

# systemctl restart postgresql
# systemctl enable postgresql

# psql -U postgres

DROP ROLE IF EXISTS "cortirio";
CREATE ROLE "cortirio" SUPERUSER PASSWORD 'c0rt1r10' LOGIN;

DROP DATABASE IF EXISTS "cortirio_proof";
CREATE DATABASE "cortirio_proof" OWNER "cortirio";

./setup_db_tables.py
```





pip3 install quart psycopg2
