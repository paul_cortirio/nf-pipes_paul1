Install Flask in a virtualenv
-------------------------------

Following http://flask.pocoo.org/docs/1.0/installation/

::


    # virtualenv flask
    New python executable in /Users/deggio/flask/flask/bin/python
    Installing setuptools, pip, wheel...
    done.
    Francescos-MacBook-Pro:flask deggio$ . flask/bin/activate
    (flask) Francescos-MacBook-Pro:flask deggio$ pip install flask
    DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7.
    Collecting flask
      Using cached https://files.pythonhosted.org/packages/7f/e7/08578774ed4536d3242b14dacb4696386634607af824ea997202cd0edb4b/Flask-1.0.2-py2.py3-none-any.whl
    Collecting Jinja2>=2.10 (from flask)
      Using cached https://files.pythonhosted.org/packages/7f/ff/ae64bacdfc95f27a016a7bed8e8686763ba4d277a78ca76f32659220a731/Jinja2-2.10-py2.py3-none-any.whl
    Collecting itsdangerous>=0.24 (from flask)
      Using cached https://files.pythonhosted.org/packages/76/ae/44b03b253d6fade317f32c24d100b3b35c2239807046a4c953c7b89fa49e/itsdangerous-1.1.0-py2.py3-none-any.whl
    Collecting Werkzeug>=0.14 (from flask)
      Using cached https://files.pythonhosted.org/packages/20/c4/12e3e56473e52375aa29c4764e70d1b8f3efa6682bef8d0aae04fe335243/Werkzeug-0.14.1-py2.py3-none-any.whl
    Collecting click>=5.1 (from flask)
      Using cached https://files.pythonhosted.org/packages/fa/37/45185cb5abbc30d7257104c434fe0b07e5a195a6847506c074527aa599ec/Click-7.0-py2.py3-none-any.whl
    Collecting MarkupSafe>=0.23 (from Jinja2>=2.10->flask)
      Using cached https://files.pythonhosted.org/packages/cd/52/927263d9cf66a12e05c5caef43ee203bd92355e9a321552d2b8c4aee5f1e/MarkupSafe-1.1.0-cp27-cp27m-macosx_10_6_intel.whl
    Installing collected packages: MarkupSafe, Jinja2, itsdangerous, Werkzeug, click, flask
    Successfully installed Jinja2-2.10 MarkupSafe-1.1.0 Werkzeug-0.14.1 click-7.0 flask-1.0.2 itsdangerous-1.1.0



Receive POST http on flask
-----------------------------

As explained in https://stackoverflow.com/questions/10434599/how-to-get-data-received-in-flask-request

::

    # cat simple.py
    from flask import Flask, abort, request
    import json

    app = Flask(__name__)


    @app.route('/foo', methods=['POST'])
    def foo():
        if not request.json:
            abort(400)
        print request.json
        return json.dumps(request.json)


    if __name__ == '__main__':
        app.run(host='0.0.0.0', port=5000, debug=True)
        
        
    # export FLASK_APP=simple.py
    # flask run
    
    # flask run
     * Serving Flask app "simple.py"
     * Environment: production
       WARNING: Do not use the development server in a production environment.
       Use a production WSGI server instead.
     * Debug mode: off
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
    {u'username': u'fizz bizz', u'userId': u'1'}
    127.0.0.1 - - [20/Feb/2019 14:04:39] "POST /foo HTTP/1.1" 200 -
     
     
     
Then in another shell:

::

    # curl -i -H "Content-Type: application/json" -X POST -d '{"userId":"1", "username": "fizz bizz"}' http://localhost:5000/foo
    HTTP/1.0 200 OK
    Content-Type: text/html; charset=utf-8
    Content-Length: 40
    Server: Werkzeug/0.14.1 Python/2.7.10
    Date: Wed, 20 Feb 2019 13:04:39 GMT

    {"username": "fizz bizz", "userId": "1"}
    
    

Nextflow sending POST to Flask
-----------------------------------

This dummy nextflow workflow will create 8 json. This is nextflow:

::

    # ./nextflow run hello.nf -with-weblog http://localhost:5000/foo
    N E X T F L O W  ~  version 19.01.0
    Launching `hello.nf` [infallible_wescoff] - revision: 361b274147
    [warm up] executor > local
    [a1/f487dd] Submitted process > splitLetters
    [19/160dec] Submitted process > convertToUpper (1)
    [c7/ee8230] Submitted process > convertToUpper (2)
    HELLO
    WORLD!


This is flask:

::

    # flask run
     * Serving Flask app "simple.py"
     * Environment: production
       WARNING: Do not use the development server in a production environment.
       Use a production WSGI server instead.
     * Debug mode: off
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
    {u'runName': u'magical_shannon', u'utcTime': u'2019-02-20T14:08:15Z', u'runStatus': u'started', u'runId': u'3e595fad-a4ab-4f0b-8f8f-f53e5aa715d2', u'event': u'started'}
    {
        "event": "started",
        "runId": "3e595fad-a4ab-4f0b-8f8f-f53e5aa715d2",
        "runName": "magical_shannon",
        "runStatus": "started",
        "utcTime": "2019-02-20T14:08:15Z"
    }
    127.0.0.1 - - [20/Feb/2019 15:08:15] "POST /foo HTTP/1.1" 200 -



So now we can receive JSON, but we have to possibly parse and manage them, then send them to DinamoDB or whatever DB. This would be an asynchronous operation, for each json.




Flask going asynchronous: Klein
----------------------------------

Following https://crossbario.com/blog/Going-Asynchronous-from-Flask-to-Twisted-Klein/.


Install what needed in the same Flask virtual environment:

::

    pip install twisted klein treq
    # python uffa.py
2019-02-20 15:42:53+0100 [-] Log opened.
2019-02-20 15:42:53+0100 [-] Site starting on 8080
2019-02-20 15:42:53+0100 [-] Starting factory <twisted.web.server.Site instance at 0x10e2369e0>
    

Same example in this page:

::

    https://klein.readthedocs.io/en/latest/examples/handlingpost.html?highlight=request#accessing-the-request-content
    
works with Nextflow when running the dummy pipeline:

::

    ./nextflow run hello -with-weblog http://localhost:8080/