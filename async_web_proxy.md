## Problem statement and analysis

We need a software to wrap the call to nextflow, so that we can extract important information from the command line the user gives.


The main idea is that the user logs into a specially crafted VM: each user should have a separate account with its own ssh key, in order to have different access permissions to bitbucket repositories and branches.
(The nextflow binary should be globally installed with sth like: `cd /usr/bin && wget -qO- https://get.nextflow.io | bash`).

Our wrapper script needs to "grep" on which "hub" the user wants to execute the pipeline.

```console
nextflow run -hub bitbucket -r master -user exactlab-demo exactlab/nf-pipes
             ^------------^ ^-------^
```

The options mean:
```
-hub
   Service hub where the project is hosted

-r, -revision
   Revision of the project to run (either a git branch, tag or commit SHA number)
```

If no revision is passed, assume branch `master`.
If a branch name is passed to the `-r` flag, extract the git commit SHA hash of the branch at that current time.

The nextflow executable doesn't seem to use ssh keys from the ssh agent, so we should manipulate the user input to our script: moreover

```console
$ ssh-keygen -t rsa -b 4096 -o -a 100 -f ~/.ssh/id_rsa -N "" -C "$(whoami)@$(hostname -s)"
```

```
# yum install -y git docker
# ssh-keyscan bitbucket.org github.com >> /etc/ssh/ssh_known_hosts
# systemctl start docker
# systemctl enable docker
```
